hello-samza
===========

**Hello Samza** is a starter project for [Apache Samza](http://samza.apache.org/) jobs.

### About

[Hello Samza](http://samza.apache.org/startup/hello-samza/latest/) is developed as part of the [Apache Samza](http://samza.apache.org) project. Please direct questions, improvements, and bug fixes there. Questions about [Hello Samza](http://samza.apache.org/startup/hello-samza/latest/) are welcome on the [dev list](http://samza.apache.org/community/mailing-lists.html) and the [Samza JIRA](https://issues.apache.org/jira/browse/SAMZA) has a hello-samza component for filing tickets.

### Instructions

The **Hello Samza** project contains example Samza applications of high-level API as well as low-level API. The following are the instructions to install the binaries and run the applications in a local Yarn cluster. See also [Hello Samza](http://samza.apache.org/startup/hello-samza/latest/) and [Hello Samza High Level API](http://samza.apache.org/learn/tutorials/latest/hello-samza-high-level-yarn.html) for more information.

#### 1. Get the Code

Check out the hello-samza project:

```
git clone https://gitlab.com/bigdatafuas/samza-hello-samza.git
cd samza-hello-samza
```

This project contains everything you'll need to run your first Samza application.

#### 2. Prepare Ubuntu 20.04 - Install Java (already done in the samza-template VM)
Install OpenJDK-8 (OpenJDK 11 seems not to work)

```
sudo apt update
sudo apt install maven openjdk-8-*
```

Add to `/etc/profile.d/java.sh`
```
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/
export PATH=$PATH:$JAVA_HOME/bin
```

Load JAVA_HOME with `source /etc/profile.d/java.sh` .

#### Prepare Ubuntu 20.04 - Install Yarn, Zookeeper, Kafka

Go into the previously cloned directory `samza-hello-samza` and install yarn, zookeeper and kafka
```
./bin/grid install yarn
./bin/grid install zookeeper
./bin/grid install kafka
```

#### Start YARN, KAFKA and Zookeeper
Go into the previously cloned directory `samza-hello-samza` and start yarn, zookeeper and kafka with
```
./bin/grid start zookeeper
./bin/grid start yarn
./bin/grid start kafka
```

### Test the applications

Go into the previously cloned directory `samza-hello-samza` and deploy/compile the applications with
```
./bin/deploy.sh
```
it packages and extracts the java files to `./deploy/samza`. Use this command also to rebuild after changing the code.

Start the application `src/main/java/samza/examples/cookbook/FilterExample.java` as a local application (i.e. without yarn):
```
./deploy/samza/bin/run-class.sh samza.examples.cookbook.FilterExampleZkLocalApplication --config job.config.loader.factory=org.apache.samza.config.loaders.PropertiesConfigLoaderFactory --config job.config.loader.properties.path=$PWD/deploy/samza/config/filter-example-local-runner.properties
```

Starting takes about half a minute, wait for 
```
JMX Server: JmxServer registry ...
```
before you continue.

In a second terminal on the same machine and in the directory `samza-hello-samza`, start a message producer:
```
./deploy/kafka/bin/kafka-console-producer.sh --topic pageview-filter-input --broker-list localhost:9092
```

In a third terminal on the same machine and in the directory `samza-hello-samza`,, start a message consumer:
```
./deploy/kafka/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic pageview-filter-output --property print.key=true
```

Paste and copy line by line into the Producer terminal: 
```
{"userId": "user1", "country": "india", "pageId":"google.com"} 
{"userId": "invalidUserId", "country": "france", "pageId":"facebook.com"}
{"userId": "user2", "country": "china", "pageId":"yahoo.com"}
```

With Ctrl^C you can stop the programs. 

Which of the messages do appear in the consumer terminal?

### Modify the application

Modify the application in `src/main/java/samza/examples/cookbook/FilterExample.java`, deploy it again and test it.


#### Full description

For a detailed description for all examples in repository visit https://github.com/apache/samza-hello-samza, this repository just contains a selected example deployed without a resource manager.
